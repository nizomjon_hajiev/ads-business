package com.example.nizomjon.adsproject.module.login.adapter;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;
import android.widget.ImageView;

import com.example.nizomjon.adsproject.model.Flower;
import com.squareup.picasso.Picasso;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class ItemBindingAdapter {


    ObservableField<Flower> flowerObservableField = new ObservableField<Flower>();
    Context context;


    public ItemBindingAdapter(Context context) {
        this.context = context;
    }

    public ObservableField<Flower> getFlowerObservableField() {
        return flowerObservableField;
    }


    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        Picasso
                .with(imageView.getContext())
                .load("http://services.hanselandpetal.com/photos/" + url)
                .into(imageView);
    }

    @BindingAdapter({"check_selection"})
    public static void check(View view, Flower flower) {
        view.setSelected(flower.isSelected());
    }


}

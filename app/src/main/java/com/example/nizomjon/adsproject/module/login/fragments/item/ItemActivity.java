package com.example.nizomjon.adsproject.module.login.fragments.item;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseActivity;
import com.example.nizomjon.adsproject.base.DaggerActivityComponent;
import com.example.nizomjon.adsproject.base.PresenterModule;
import com.example.nizomjon.adsproject.databinding.FragmentItemBinding;
import com.example.nizomjon.adsproject.event.ItemEvent;
import com.example.nizomjon.adsproject.model.Flower;
import com.example.nizomjon.adsproject.module.login.adapter.ItemBindingAdapter;
import com.example.nizomjon.adsproject.utils.UtilsModule;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Nizomjon on 11/22/16.
 */

public class ItemActivity extends BaseActivity {
    public final static String EXTRA_SHOT = "EXTRA_SHOT";
    public final static String RESULT_EXTRA_SHOT_ID = "RESULT_EXTRA_SHOT_ID";
    private static final int RC_LOGIN_LIKE = 0;
    private static final int RC_LOGIN_COMMENT = 1;
    private static final float SCRIM_ADJUSTMENT = 0.075f;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.image)
    ImageView imageView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private static final String EXTRA_BODY = "nizzle.extraBody";


    public static void navigate(AppCompatActivity activity, View transitionImage, String flower) {
        Intent intent = new Intent(activity, ItemActivity.class);
        intent.putExtra(EXTRA_BODY, flower);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, "image");
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityTransitions();
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        init();


    }


    private void init() {
        setHomeAsUp();
        setSupportActionBar(toolbar);
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), "image");
        supportPostponeEnterTransition();
        String itemTitle = getIntent().getStringExtra(EXTRA_BODY);
        Flower flower = gson.fromJson(itemTitle, new TypeToken<Flower>() {
        }.getType());
        FragmentItemBinding binding = DataBindingUtil.setContentView(this, R.layout.fragment_item);
        ItemBindingAdapter bindingAdapter = new ItemBindingAdapter(this);
        bindingAdapter.getFlowerObservableField().set(flower);
        binding.setViewModel(bindingAdapter);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        Picasso.with(this).load("http://services.hanselandpetal.com/photos/" + flower.getPhoto()).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                    public void onGenerated(Palette palette) {
                        applyPalette(palette);
                    }
                });
            }

            @Override
            public void onError() {

            }
        });
    }

    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    private void applyPalette(Palette palette) {
        int primaryDark = getResources().getColor(R.color.colorPrimaryDark);
        int primary = getResources().getColor(R.color.colorPrimary);
        collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(primary));
        collapsingToolbarLayout.setStatusBarScrimColor(palette.getDarkMutedColor(primaryDark));
        updateBackground(fab, palette);
        supportStartPostponedEnterTransition();
    }

    private void updateBackground(FloatingActionButton fab, Palette palette) {
        int lightVibrantColor = palette.getLightVibrantColor(getResources().getColor(android.R.color.white));
        int vibrantColor = palette.getVibrantColor(getResources().getColor(R.color.colorAccent));

        fab.setRippleColor(lightVibrantColor);
        fab.setBackgroundTintList(ColorStateList.valueOf(vibrantColor));
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_item;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe
    public void onEvent(ItemEvent event) {

    }
}

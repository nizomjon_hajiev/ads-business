package com.example.nizomjon.adsproject.module.login.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.ViewFlipper;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseActivity;
import com.example.nizomjon.adsproject.base.DaggerActivityComponent;
import com.example.nizomjon.adsproject.base.PresenterModule;
import com.example.nizomjon.adsproject.event.ItemEvent;
import com.example.nizomjon.adsproject.module.login.advertisement.AddAdsActivity;
import com.example.nizomjon.adsproject.module.login.fragments.category.CategoryFragment;
import com.example.nizomjon.adsproject.module.login.fragments.favorite.FavoritesFragment;
import com.example.nizomjon.adsproject.module.login.fragments.profile.ProfileFragment;
import com.example.nizomjon.adsproject.module.login.fragments.timeline.TimelineFragment;
import com.example.nizomjon.adsproject.utils.UtilsModule;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class MainActivity extends BaseActivity implements MainView {

    public static final int TAB_FEED = 0;
    public static final int TAB_CATALOG = 1;
    public static final int TAB_FAVORITE = 2;
    public static final int TAB_PROFILE = 3;

    private final String FRAGMENT_TIMELINE = "fragment_timeline";
    private final String FRAGMENT_CATALOG = "fragment_catalog";
    private final String FRAGMENT_FAVORITES = "fragment_favorites";
    private final String FRAGMENT_PROFILE = "fragment_profile";
    TimelineFragment timelineFragment;
    CategoryFragment categoryFragment;
    FavoritesFragment favoritesFragment;
    ProfileFragment profileFragment;


    @Inject
    MainPresenter mainPresenter;

    @BindView(R.id.bottom_navigation)
    BottomBar bottomBar;

    @Nullable
    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;

    private int mCurrentTabIndex = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        mainPresenter.attachView(this);

        if (savedInstanceState != null) {
            mCurrentTabIndex = savedInstanceState.getInt("CURRENT_TAB");
        }
        bottomBar.selectTabAtPosition(mCurrentTabIndex);
        init();

    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }


    @Override
    public void init() {
        bottomBar.setDefaultTabPosition(0);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {

                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                switch (tabId) {

                    case R.id.action_feed: {
                        if (timelineFragment == null) {
                            Fragment fragment = fm.findFragmentByTag(FRAGMENT_TIMELINE);
                            if (fragment != null) {
                                timelineFragment = (TimelineFragment) fragment;
                            } else {
                                timelineFragment = new TimelineFragment();
                                transaction.add(R.id.feed_container, timelineFragment, FRAGMENT_TIMELINE).commit();
                            }
                        }
                        viewFlipper.setDisplayedChild(TAB_FEED);
                        break;
                    }

                    case R.id.action_category: {
                        if (categoryFragment == null) {
                            Fragment fragment = fm.findFragmentByTag(FRAGMENT_CATALOG);
                            if (fragment != null) {
                                categoryFragment = (CategoryFragment) fragment;
                            } else {
                                categoryFragment = new CategoryFragment();
                                transaction.add(R.id.categories_container, categoryFragment, FRAGMENT_CATALOG).commit();
                            }
                        }
                        viewFlipper.setDisplayedChild(TAB_CATALOG);
                        break;
                    }

                    case R.id.action_add: {
                        Intent intent = new Intent(MainActivity.this, AddAdsActivity.class);
                        startActivity(intent);
//                        appUtils.showToast("Add");
                        break;
                    }
                    case R.id.action_favorites: {
                        if (favoritesFragment == null) {
                            Fragment fragment = fm.findFragmentByTag(FRAGMENT_FAVORITES);
                            if (fragment != null) {
                                favoritesFragment = (FavoritesFragment) fragment;
                            } else {
                                favoritesFragment = new FavoritesFragment();
                                transaction.add(R.id.favorite_container, favoritesFragment, FRAGMENT_FAVORITES).commit();
                            }
                        }
                        viewFlipper.setDisplayedChild(TAB_FAVORITE);
                        break;
                    }

                    case R.id.action_profile: {
                        if (profileFragment == null) {
                            Fragment fragment = fm.findFragmentByTag(FRAGMENT_PROFILE);
                            if (fragment != null) {
                                profileFragment = (ProfileFragment) fragment;
                            } else {
                                profileFragment = new ProfileFragment();
                                transaction.add(R.id.profile_container, profileFragment, FRAGMENT_PROFILE).commit();
                            }
                        }
                        viewFlipper.setDisplayedChild(TAB_PROFILE);
                        break;
                    }

                }

            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                switch (tabId) {

                    case R.id.action_feed: {
//                       timelineFragment.scrollToTop();
                    }
                    break;
                    case R.id.action_category: {
                        appUtils.showToast("Reselected");
                    }
                    break;
                    case R.id.action_favorites: {

                    }
                    break;
                    case R.id.action_profile: {

                    }
                    break;
                }
            }
        });

        bottomBar.getTabWithId(R.id.action_feed).setBadgeCount(4);
        bottomBar.getTabWithId(R.id.action_category).setBadgeCount(3);
    }

    @Override
    public void replaceFragment() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {

    }

    public void onBackPressed() {
        // Catch back action and pops from backstack
        // (if you called previously to addToBackStack() in your transaction)
//        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
//            getSupportFragmentManager().popBackStack();
//        }
        // Default action on back pressed
        super.onBackPressed();
    }

    @Subscribe
    public void onEvent(ItemEvent event) {
        if (event.getType() == ItemEvent.Type.ITEM_ADDED) {

        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (viewFlipper != null) {
            outState.putInt("CURRENT_TAB", viewFlipper.getDisplayedChild());
        }
    }


}

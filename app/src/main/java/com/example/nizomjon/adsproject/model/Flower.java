package com.example.nizomjon.adsproject.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Bitmap;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class Flower extends BaseObservable {

    @Bindable
    private int productId;
    @Bindable
    private String name;
    @Bindable
    private String category;
    @Bindable
    private String instructions;
    @Bindable
    private double price;
    @Bindable
    private String photo;
    @Bindable
    private Bitmap bitmap;
    @Bindable
    boolean isSelected;

    public Flower() {
    }

    public Flower(String name, String instructions, String photo) {
        this.name = name;
        this.instructions = instructions;
        this.photo = photo;
    }

    public int getProductId() {
        return productId;
    }
    public void setProductId(int productId) {
        this.productId = productId;
    }
    @Bindable
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getInstructions() {
        return instructions;
    }
    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getPhoto() {
        return photo;
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public Bitmap getBitmap() {
        return bitmap;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
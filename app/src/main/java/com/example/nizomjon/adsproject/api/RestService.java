package com.example.nizomjon.adsproject.api;

import com.example.nizomjon.adsproject.model.Flower;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Nizomjon on 11/18/16.
 */

public interface RestService {

    @GET("/feeds/flowers.json")
    Observable<List<Flower>> getFlowers();

}

package com.example.nizomjon.adsproject.module.login.advertisement;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseActivity;
import com.example.nizomjon.adsproject.base.DaggerActivityComponent;
import com.example.nizomjon.adsproject.base.PresenterModule;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import javax.inject.Inject;

import butterknife.OnClick;

/**
 * Created by Nizomjon on 11/29/16.
 */

public class AddAdsActivity extends BaseActivity implements AddAdsView {

    @Inject
    AddAdsPresenter presenter;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        presenter.attachView(this);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        setHomeAsUp();
    }

    @OnClick(R.id.imageContainer)
    void moveToGallery(){
//        Intent intent = new Intent(AddAdsActivity.this, GalleryActivity.class);
//        startActivity(intent);
    }
    @Override
    public int getLayout() {
        return R.layout.activity_addads;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {

    }
}

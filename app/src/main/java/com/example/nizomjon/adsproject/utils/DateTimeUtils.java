package com.example.nizomjon.adsproject.utils;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Nizomjon on 11/18/16.
 */

public class DateTimeUtils {


    public String getDateFromTimestamp(String timestamp) {

        long time = Long.parseLong(timestamp) * 1000;
        try {
            DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            Date netDate = (new Date(time));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx xxxx xxxx";
        }
    }


    public long convertDateToTimeStamp(String dateToConvert, String dateFormat) {
        DateFormat formatter = new SimpleDateFormat(dateFormat, Locale.getDefault());
        Date date = null;
        try {
            date = formatter.parse(dateToConvert);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static long getTimeStampInSeconds() {
        return System.currentTimeMillis() / 1000;
    }

    public long getTimeOffset() {
        long currentTime = System.currentTimeMillis();
        int edtOffset = TimeZone.getTimeZone("Your Time Zone").getOffset(currentTime);
        int current = TimeZone.getDefault().getOffset(currentTime);
        return current - edtOffset;
    }

    @SuppressLint("SimpleDateFormat")
    public String convertDateOneToAnother(String dateToConvert, String formatFrom, String formatTo) {
        String outputDateStr = null;
        DateFormat inputFormat = new SimpleDateFormat(formatFrom);
        DateFormat outputFormat = new SimpleDateFormat(formatTo);
        Date date;
        try {
            date = inputFormat.parse(dateToConvert);
            outputDateStr = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDateStr;
    }

}

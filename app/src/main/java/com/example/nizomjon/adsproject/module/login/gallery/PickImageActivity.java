package com.example.nizomjon.adsproject.module.login.gallery;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseActivity;
import com.example.nizomjon.adsproject.utils.ImagePickHelper;

/**
 * Created by Nizomjon on 11/29/16.
 */

public abstract class PickImageActivity extends BaseActivity {

    public static final int REQUEST_READ_STORAGE_PERMISSION = 1;
    public static final int REQUEST_CAMERA_PERMISSION = 2;

    private ImagePickHelper mImagePickerHelper;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (mImagePickerHelper != null && mImagePickerHelper.getRequestCode() == requestCode) {
                mImagePickerHelper.onActivityResult(requestCode, resultCode, data);
                if (mImagePickerHelper.isAccessDenied() && Build.VERSION.SDK_INT >= 23
                        && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_READ_STORAGE_PERMISSION);
                } else {
                    onPickedImageReady(mImagePickerHelper.getPickedImageFilePath());
                    mImagePickerHelper = null;
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull
                                                   String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_READ_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mImagePickerHelper != null) {
                        mImagePickerHelper.accessGranted();
                        onPickedImageReady(mImagePickerHelper.getPickedImageFilePath());
                        mImagePickerHelper = null;
                    }
                }
            }
            case REQUEST_CAMERA_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (mImagePickerHelper != null) {
                        mImagePickerHelper.pick();
                    }

                } else {

                    Toast.makeText(PickImageActivity.this, "Denied", Toast.LENGTH_SHORT).show();

                }
                break;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save image picker for restore (Some times activity removed on open camera)
        if (mImagePickerHelper != null) {
            outState.putBoolean("RESTORE_CREATE_POST_IMAGE_PICKER", true);
            mImagePickerHelper.onSaveInstanceState(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null
                && savedInstanceState.getBoolean("RESTORE_CREATE_POST_IMAGE_PICKER")) {
            mImagePickerHelper = new ImagePickHelper(this, savedInstanceState);
        }
    }

    protected void pickImage(int requestCode, boolean isPhotoPost) {
        mImagePickerHelper = new ImagePickHelper(this, requestCode, getString(R.string.photo), isPhotoPost);

        if (!checkPermission()) {

            requestPermission();

        } else {

            mImagePickerHelper.pick();

        }


    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;

        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);

//            Toast.makeText(this, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);

        }
    }


    protected abstract void onPickedImageReady(String filePath);
}

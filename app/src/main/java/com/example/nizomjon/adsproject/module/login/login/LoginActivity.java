package com.example.nizomjon.adsproject.module.login.login;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseActivity;
import com.example.nizomjon.adsproject.utils.AppUtils;

import javax.inject.Inject;

/**
 * Created by Nizomjon on 11/18/16.
 */

public class LoginActivity extends BaseActivity implements LoginView {

    @Inject
    LoginPresenter loginPresenter;

    @Inject
    AppUtils utils;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginPresenter.attachView(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void onLoginSuccess() {

    }

    @Override
    public void closeApplication() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {

    }
}

package com.example.nizomjon.adsproject.model;

/**
 * Created by Nizomjon on 11/30/16.
 */

public class GalleryItem {
    private String path;
    private boolean isSelected;

    public GalleryItem() {
    }

    public GalleryItem(String path, boolean isSelected) {
        this.path = path;
        this.isSelected = isSelected;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

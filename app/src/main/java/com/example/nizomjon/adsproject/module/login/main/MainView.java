package com.example.nizomjon.adsproject.module.login.main;

import com.example.nizomjon.adsproject.mvp.MvpView;

/**
 * Created by Nizomjon on 11/21/16.
 */

interface MainView extends MvpView {



    void init();

    void replaceFragment();


}

package com.example.nizomjon.adsproject.module.login.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.nizomjon.adsproject.BR;
import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.model.Flower;
import com.example.nizomjon.adsproject.module.login.fragments.item.ItemActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    List<Flower> flowers;
    Context context;
    ItemClickListener itemClickListener;
    private boolean isCategory;

    public ItemAdapter(List<Flower> flowers, Context context, ItemClickListener itemClickListener) {
        this.flowers = flowers;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    public ItemAdapter(List<Flower> flowers, Context context, ItemClickListener itemClickListener, boolean isCategory) {
        this.flowers = flowers;
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.isCategory = isCategory;
    }

    public ItemAdapter(Context context, ItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(isCategory ? R.layout.category_item : R.layout.feed_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Flower flower = flowers.get(position);
        holder.bind(flower, context);

    }

    public void add(Flower flower, int position) {
        if (flowers == null) {
            flowers = new ArrayList<>();
        }
        flowers.add(flower);
        notifyDataSetChanged();
//        notifyItemInserted(position);
    }

    public void remove(Flower flower, int position) {
        if (flowers != null) {
            flowers.remove(flower);
        }
        notifyItemRemoved(position);

    }

    public void notifyItem(Flower flower, int position) {
        if (flower.isSelected()) {
            flower.setSelected(false);
        } else {
            flower.setSelected(true);
        }
        notifyItemChanged(position, flower);
    }

    public interface ItemClickListener {

        void clickItem(Flower flower, int position);
    }

    public void clear() {
        if (flowers != null)
            flowers.clear();
    }

    public void addAll(List<Flower> flowerList) {
        if (flowers == null) {
            flowers = new ArrayList<>();
        }

        flowers.addAll(flowerList);
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return flowers.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        Flower flower;
        Context context;

        @Nullable
        @BindView(R.id.image)
        ImageView imageView;
        Gson gson;

        @OnClick(R.id.image)
        void click() {

            ItemActivity.navigate((AppCompatActivity) context, imageView, gson.toJson(flower));
        }

//        @OnClick(R.id.favorite_button)
//        void favorite() {
//            if (itemClickListener != null) {
//                itemClickListener.clickItem(flower, getAdapterPosition());
//            }
//        }


        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            binding = DataBindingUtil.bind(itemView);
            gson = new Gson();
        }

        public void bind(Flower flower, Context context) {
            this.flower = flower;
            this.context = context;
            ItemBindingAdapter itemBindingAdapter = new ItemBindingAdapter(context);
            itemBindingAdapter.getFlowerObservableField().set(flower);
            binding.setVariable(BR.viewModel, itemBindingAdapter);
            binding.executePendingBindings();
        }

        public Flower getFlower() {
            return flower;
        }


        ViewDataBinding binding;
    }

}

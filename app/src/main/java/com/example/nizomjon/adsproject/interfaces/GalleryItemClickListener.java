package com.example.nizomjon.adsproject.interfaces;

/**
 * Created by Nizomjon on 11/30/16.
 */

public interface GalleryItemClickListener {
    void selectImage(String path, int position);
}

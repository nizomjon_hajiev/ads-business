package com.example.nizomjon.adsproject.module.login.register;

import android.content.Context;

import com.example.nizomjon.adsproject.api.NetModule;
import com.example.nizomjon.adsproject.api.RestService;
import com.example.nizomjon.adsproject.base.DaggerPresenterComponent;
import com.example.nizomjon.adsproject.module.login.login.LoginView;
import com.example.nizomjon.adsproject.mvp.BasePresenter;
import com.example.nizomjon.adsproject.utils.AppUtils;
import com.example.nizomjon.adsproject.utils.PreferenceManager;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Scheduler;

/**
 * Created by Nizomjon on 11/18/16.
 */
public class RegisterPresenter extends BasePresenter<LoginView> {

    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;

    @Inject
    RestService mRestService;

    @Inject
    PreferenceManager mprefs;

    @Inject
    AppUtils mUtils;

    public RegisterPresenter(Context context) {
        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context)).build().inject(this);
    }


}

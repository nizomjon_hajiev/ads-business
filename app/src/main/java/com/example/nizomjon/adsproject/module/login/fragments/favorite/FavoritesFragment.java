package com.example.nizomjon.adsproject.module.login.fragments.favorite;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseFragment;
import com.example.nizomjon.adsproject.base.DaggerActivityComponent;
import com.example.nizomjon.adsproject.base.PresenterModule;
import com.example.nizomjon.adsproject.event.ItemEvent;
import com.example.nizomjon.adsproject.module.login.adapter.ItemAdapter;
import com.example.nizomjon.adsproject.utils.AppUtils;
import com.example.nizomjon.adsproject.utils.CacheUtils;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import rx.Scheduler;
import rx.Subscription;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class FavoritesFragment extends BaseFragment {

    private Subscription subscription;

//    @Inject
//    RxBus rxBus;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Nullable
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    AppUtils appUtils;
    @Inject
    CacheUtils cacheUtils;
    ItemAdapter itemAdapter;

    @Inject
    @Named("main_thread")
    Scheduler mMainThread;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);
    }


    @Override
    public void onResume() {
        super.onResume();
//        autoUnsubscribe();

    }

    public void handlerBus(Object o) {
        if (o instanceof ItemEvent) {
            if (((ItemEvent) o).getType() == ItemEvent.Type.ITEM_ADDED) {
                init();
            } else if (((ItemEvent) o).getType() == ItemEvent.Type.ITEM_REMOVED) {
                init();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        autoUnsubscribe();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        reloadContent();
        swipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            itemAdapter.notifyDataSetChanged();
            reloadContent();
        });
    }

    private void reloadContent() {
        itemAdapter = new ItemAdapter(cacheUtils.getFavoritesFeeds(), getActivity(), null, true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(itemAdapter);
    }

    private void autoUnsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_favorites;
    }

    @Subscribe
    public void onEvent(ItemEvent event) {

    }

}

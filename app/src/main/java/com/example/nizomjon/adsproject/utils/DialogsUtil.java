package com.example.nizomjon.adsproject.utils;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.interfaces.OnDialogButtonClickListener;

/**
 * Created by Nizomjon on 11/18/16.
 */

public class DialogsUtil {

    private Context mContext;

    public DialogsUtil(Context context) {
        this.mContext = context;
    }

    public void openAlertDialog(Context context, String message, String positiveBtnText, String negativeBtnText,
                                final OnDialogButtonClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(positiveBtnText, (dialog, which) -> {
            dialog.dismiss();
            listener.onPositiveButtonClicked();

        });

        builder.setNegativeButton(negativeBtnText, (dialog, which) -> {
            dialog.dismiss();
            listener.onNegativeButtonClicked();

        });
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(message);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setCancelable(false);
        builder.create().show();
    }



}

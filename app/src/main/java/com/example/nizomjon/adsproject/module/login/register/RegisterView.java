package com.example.nizomjon.adsproject.module.login.register;

import com.example.nizomjon.adsproject.mvp.MvpView;

/**
 * Created by Nizomjon on 11/18/16.
 */

public interface RegisterView extends MvpView {


    void onLoginSuccess();

    void closeApplication();
}

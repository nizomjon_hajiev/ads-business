package com.example.nizomjon.adsproject.event;

import com.example.nizomjon.adsproject.model.Flower;

import java.util.List;

/**
 * Created by Nizomjon on 11/22/16.
 */

public class FavoriteEvent {

    private Type type;
    private List<Flower> flowers;
    private int position;


    public static FavoriteEvent update(List<Flower> flowers , int position){

        FavoriteEvent event = new FavoriteEvent();
        event.type=Type.ITEM_UPDATE;
        event.position = position;
        event.flowers = flowers;
        return event;
    }




    public enum Type{
        ITEM_UPDATE,
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public int getPosition() {
        return position;
    }
}

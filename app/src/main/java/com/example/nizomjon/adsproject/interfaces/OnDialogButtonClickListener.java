package com.example.nizomjon.adsproject.interfaces;

/**
 * Created by Nizomjon on 11/18/16.
 */

public interface OnDialogButtonClickListener {
    void onPositiveButtonClicked();

    void onNegativeButtonClicked();
}

package com.example.nizomjon.adsproject.base;

import com.example.nizomjon.adsproject.api.NetModule;
import com.example.nizomjon.adsproject.module.login.advertisement.AddAdsPresenter;
import com.example.nizomjon.adsproject.module.login.fragments.category.CategoryPresenter;
import com.example.nizomjon.adsproject.module.login.fragments.timeline.TimelinePresenter;
import com.example.nizomjon.adsproject.module.login.login.LoginPresenter;
import com.example.nizomjon.adsproject.module.login.main.MainPresenter;
import com.example.nizomjon.adsproject.module.login.register.RegisterPresenter;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 11/18/16.
 */

@Singleton
@Component(modules = {NetModule.class, UtilsModule.class})
public interface PresenterComponent {
    void inject(LoginPresenter presenter);
    void inject(RegisterPresenter presenter);
    void inject(MainPresenter presenter);
    void inject(TimelinePresenter presenter);
    void inject(CategoryPresenter presenter);
    void inject(AddAdsPresenter presenter);
}

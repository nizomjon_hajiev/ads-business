package com.example.nizomjon.adsproject.module.login.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.interfaces.GalleryItemClickListener;
import com.example.nizomjon.adsproject.model.GalleryItem;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by Nizomjon on 11/30/16.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private Context context;
    private List<GalleryItem> items;
    private GalleryItemClickListener itemClickListener;

    public GalleryAdapter(Context context, List<GalleryItem> items, GalleryItemClickListener itemClickListener) {
        this.context = context;
        this.items = items;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.gallery_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        if (items != null && items.size() > 0) {
            return items.size();
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}

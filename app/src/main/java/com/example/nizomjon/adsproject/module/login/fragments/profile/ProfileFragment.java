package com.example.nizomjon.adsproject.module.login.fragments.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseFragment;
import com.example.nizomjon.adsproject.base.DaggerActivityComponent;
import com.example.nizomjon.adsproject.base.PresenterModule;
import com.example.nizomjon.adsproject.event.ItemEvent;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class ProfileFragment extends BaseFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);
    }

    public void handlerBus(Object o) {

    }
    @Subscribe
    public void onEvent(ItemEvent event){

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile;
    }
}

package com.example.nizomjon.adsproject.base;

import android.app.Application;

import com.example.nizomjon.adsproject.BuildConfig;
import com.example.nizomjon.adsproject.utils.RxBus;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import timber.log.Timber;

/**
 * Created by Nizomjon on 11/18/16.
 */

public class AdsApplication extends Application {


    private AppComponent mAppComponent;

    private static AdsApplication instance;

    private RxBus bus;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder().utilsModule(new UtilsModule(this)).build();
        instance = this;
        bus = new RxBus();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static AdsApplication get() {
        return instance;
    }

    public RxBus bus() {
        return bus;
    }
}

package com.example.nizomjon.adsproject.module.login.fragments.timeline;

import com.example.nizomjon.adsproject.model.Flower;
import com.example.nizomjon.adsproject.mvp.MvpView;

import java.util.List;

/**
 * Created by Nizomjon on 11/21/16.
 */

public interface TimelineView extends MvpView {

    void showList(List<Flower> flowers);

//    void clickItem(Flower flower);

}

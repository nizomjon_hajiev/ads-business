package com.example.nizomjon.adsproject.module.login.fragments.timeline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.AdsApplication;
import com.example.nizomjon.adsproject.base.BaseFragment;
import com.example.nizomjon.adsproject.base.DaggerActivityComponent;
import com.example.nizomjon.adsproject.base.PresenterModule;
import com.example.nizomjon.adsproject.event.FavoriteEvent;
import com.example.nizomjon.adsproject.event.ItemEvent;
import com.example.nizomjon.adsproject.model.Flower;
import com.example.nizomjon.adsproject.module.login.adapter.ItemAdapter;
import com.example.nizomjon.adsproject.utils.AppUtils;
import com.example.nizomjon.adsproject.utils.CacheUtils;
import com.example.nizomjon.adsproject.utils.UtilsModule;
import com.example.nizomjon.adsproject.widgets.CustomLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class TimelineFragment extends BaseFragment implements TimelineView {

    @Inject
    TimelinePresenter presenter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Nullable
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    AppUtils appUtils;

    @Inject
    CacheUtils cacheUtils;

    List<Flower> favoritesFlowers;

    ItemAdapter itemAdapter;

    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);
        presenter.attachView(this);

    }

    @Override
    public void handlerBus(Object o) {

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        presenter.getList();
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_timeline;
    }


    @Override
    public void showList(List<Flower> flowers) {
        itemAdapter = new ItemAdapter(flowers, getActivity(), clickListener);
        CustomLayoutManager staggeredGridLayoutManager = new CustomLayoutManager(2, 1);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(itemAdapter);
    }

    private void init() {
        favoritesFlowers = new ArrayList<>();
        CustomLayoutManager staggeredGridLayoutManager = new CustomLayoutManager(2, 1);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        swipeRefreshLayout.requestDisallowInterceptTouchEvent(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            itemAdapter.notifyDataSetChanged();
            reloadContent();
        });
    }

    private void reloadContent() {
        presenter.getList();
    }

    ItemAdapter.ItemClickListener clickListener = new ItemAdapter.ItemClickListener() {
        @Override
        public void clickItem(Flower flower, int position) {
            if (flower.isSelected()) {
                AdsApplication.get().bus().send(ItemEvent.remove(flower, position));
                Observable.from(cacheUtils.getFavoritesFeeds())
                        .filter(flower1 -> flower.getProductId() == flower1.getProductId())
                        .toBlocking()
                         .subscribe(flower12 -> {
                            favoritesFlowers.remove(flower12);
                        });
                EventBus.getDefault().post(FavoriteEvent.update(favoritesFlowers, position));
            } else {
                AdsApplication.get().bus().send(ItemEvent.add(flower, position));
                favoritesFlowers.add(flower);
                EventBus.getDefault().post(FavoriteEvent.update(favoritesFlowers, position));
            }
            itemAdapter.notifyItem(flower, position);
        }
    };

    public void scrollToTop() {
        recyclerView.smoothScrollToPosition(1);
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void setError(String error) {
        appUtils.showToast(error);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Subscribe
    public void onEvent(ItemEvent event) {

    }
}

package com.example.nizomjon.adsproject.utils;

import android.content.Context;

import com.example.nizomjon.adsproject.event.FavoriteEvent;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Nizomjon on 11/18/16.
 */

@Module
public class UtilsModule {

    private Context mContext;

    public UtilsModule(Context context) {
        this.mContext = context;
    }


    @Provides
    @Singleton
    AppUtils getAppUtils() {
        return new AppUtils(mContext);
    }

    @Provides
    @Singleton
    DateTimeUtils getDateTimeUtils() {
        return new DateTimeUtils();
    }

    @Provides
    @Singleton
    DialogsUtil getDialogUtils() {
        return new DialogsUtil(mContext);
    }

    @Provides
    @Singleton
    @Named("new_thread")
    Scheduler getNewThread() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @Named("main_thread")
    Scheduler getMainThread() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    PreferenceManager getPreferences() {
        return new PreferenceManager(mContext);
    }

    @Provides
    @Singleton
    FragmentUtils getFragUtils() {
        return new FragmentUtils();
    }

    @Provides
    @Singleton
    RxBus getRxBus() {
        return new RxBus();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    CacheUtils provideCacheUtils(Gson gson) {
        CacheUtils cacheUtils = new CacheUtils(mContext, gson);
        EventBus.getDefault().register(cacheUtils);
        return cacheUtils;
    }

    @Subscribe
    public void onEvent(FavoriteEvent event){

    }
}

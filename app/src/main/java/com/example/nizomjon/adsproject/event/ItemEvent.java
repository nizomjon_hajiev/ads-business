package com.example.nizomjon.adsproject.event;

import com.example.nizomjon.adsproject.model.Flower;

/**
 * Created by Nizomjon on 11/22/16.
 */

public class ItemEvent {

    private Type type;
    private Flower flower;
    private int position;


    public static ItemEvent add(Flower flower , int position){

        ItemEvent event = new ItemEvent();
        event.type=Type.ITEM_ADDED;
        event.position = position;
        event.flower = flower;
        return event;
    }


    public static ItemEvent remove(Flower flower , int position){
        ItemEvent event = new ItemEvent();
        event.type=Type.ITEM_REMOVED;
        event.flower = flower;
        event.position = position;
        return event;
    }



    public enum Type{
        ITEM_ADDED,
        ITEM_REMOVED
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Flower getFlower() {
        return flower;
    }

    public void setFlower(Flower flower) {
        this.flower = flower;
    }

    public int getPosition() {
        return position;
    }
}

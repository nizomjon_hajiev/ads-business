package com.example.nizomjon.adsproject.module.login.fragments.category;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nizomjon.adsproject.R;
import com.example.nizomjon.adsproject.base.BaseFragment;
import com.example.nizomjon.adsproject.base.DaggerActivityComponent;
import com.example.nizomjon.adsproject.base.PresenterModule;
import com.example.nizomjon.adsproject.event.ItemEvent;
import com.example.nizomjon.adsproject.model.Flower;
import com.example.nizomjon.adsproject.module.login.adapter.ItemAdapter;
import com.example.nizomjon.adsproject.utils.UtilsModule;
import com.example.nizomjon.adsproject.widgets.PrecachingLayoutManager;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import rx.Scheduler;
import rx.Subscription;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class CategoryFragment extends BaseFragment implements CategoryView {

    @Inject
    CategoryPresenter presenter;
    @Inject
    @Named("main_thread")
    Scheduler mMainThread;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private Subscription subscription;
    private ItemAdapter itemAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);
        presenter.attachView(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void handlerBus(Object o) {
        if (o instanceof ItemEvent) {
            if (((ItemEvent) o).getType() == ItemEvent.Type.ITEM_ADDED) {
//                itemAdapter.add(((ItemEvent) o).getFlower(), ((ItemEvent) o).getPosition());
//                recyclerView.setAdapter(itemAdapter);
            } else if (((ItemEvent) o).getType() == ItemEvent.Type.ITEM_REMOVED) {
//                itemAdapter.remove(((ItemEvent) o).getFlower(), ((ItemEvent) o).getPosition());
            }
        }
    }

    ItemAdapter.ItemClickListener clickListener = new ItemAdapter.ItemClickListener() {
        @Override
        public void clickItem(Flower flower, int position) {
//            if (flower.isSelected()) {
////                AdsApplication.get().bus().send(ItemEvent.remove(flower));
//            } else {
////                AdsApplication.get().bus().send(ItemEvent.add(flower));
//            }
//            itemAdapter.notifyItem(flower, position);
        }
    };

    @Override
    public void onResume() {
        super.onResume();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        reloadContent();


    }

    private void reloadContent() {
        presenter.getCategories();
    }

    private void init() {
        PrecachingLayoutManager layoutManager = new PrecachingLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_category;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setError(String error) {

    }

    @Subscribe
    public void onEvent(ItemEvent event) {
        if (event.getType() == ItemEvent.Type.ITEM_ADDED) {

        }

    }

    @Override
    public void showCategories(List<Flower> flowers) {
        itemAdapter = new ItemAdapter(flowers, getActivity(), clickListener, true);
        recyclerView.setAdapter(itemAdapter);
    }
}

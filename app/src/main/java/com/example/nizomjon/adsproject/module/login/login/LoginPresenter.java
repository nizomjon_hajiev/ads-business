package com.example.nizomjon.adsproject.module.login.login;

import android.content.Context;

import com.example.nizomjon.adsproject.api.NetModule;
import com.example.nizomjon.adsproject.api.RestService;
import com.example.nizomjon.adsproject.base.DaggerPresenterComponent;
import com.example.nizomjon.adsproject.mvp.BasePresenter;
import com.example.nizomjon.adsproject.utils.AppUtils;
import com.example.nizomjon.adsproject.utils.PreferenceManager;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Scheduler;

/**
 * Created by Nizomjon on 11/18/16.
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;

    @Inject
    RestService api;

    @Inject
    PreferenceManager mprefs;

    @Inject
    AppUtils mUtils;

    public LoginPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context)).build().inject(this);
    }

    private void login(String username,String phoneNumber){

        getMvpView().showProgress();

//        api


    }


}

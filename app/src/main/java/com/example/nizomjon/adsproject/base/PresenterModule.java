package com.example.nizomjon.adsproject.base;

import android.content.Context;

import com.example.nizomjon.adsproject.module.login.advertisement.AddAdsPresenter;
import com.example.nizomjon.adsproject.module.login.fragments.category.CategoryPresenter;
import com.example.nizomjon.adsproject.module.login.fragments.timeline.TimelinePresenter;
import com.example.nizomjon.adsproject.module.login.login.LoginPresenter;
import com.example.nizomjon.adsproject.module.login.main.MainPresenter;
import com.example.nizomjon.adsproject.module.login.register.RegisterPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nizomjon on 11/18/16.
 */

@Module
public class PresenterModule {

    private Context context;

    public PresenterModule(Context context) {
        this.context = context;
    }

    @Provides
    LoginPresenter loginPresenter() {
        return new LoginPresenter(context);
    }

    @Provides
    RegisterPresenter registerPresenter() {
        return new RegisterPresenter(context);
    }

    @Provides
    MainPresenter mainPresenter() {
        return new MainPresenter(context);
    }

    @Provides
    TimelinePresenter timelinePresenter() {
        return new TimelinePresenter(context);
    }

    @Provides
    CategoryPresenter categoryPresenter() {
        return new CategoryPresenter(context);
    }

    @Provides
    AddAdsPresenter addAdsPresenter(){return new AddAdsPresenter(context);}
}

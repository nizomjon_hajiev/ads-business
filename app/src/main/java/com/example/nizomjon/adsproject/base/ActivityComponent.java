package com.example.nizomjon.adsproject.base;

import com.example.nizomjon.adsproject.module.login.advertisement.AddAdsActivity;
import com.example.nizomjon.adsproject.module.login.fragments.category.CategoryFragment;
import com.example.nizomjon.adsproject.module.login.fragments.favorite.FavoritesFragment;
import com.example.nizomjon.adsproject.module.login.fragments.item.ItemActivity;
import com.example.nizomjon.adsproject.module.login.fragments.profile.ProfileFragment;
import com.example.nizomjon.adsproject.module.login.fragments.timeline.TimelineFragment;
import com.example.nizomjon.adsproject.module.login.login.LoginActivity;
import com.example.nizomjon.adsproject.module.login.main.MainActivity;
import com.example.nizomjon.adsproject.module.login.register.RegisterActivity;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 11/18/16.
 */
@Singleton
@Component(modules = {PresenterModule.class, UtilsModule.class})
public interface ActivityComponent {
    void inject(BaseActivity activity);

    void inject(LoginActivity activity);

    void inject(RegisterActivity activity);

    void inject(MainActivity activity);
    void inject(ItemActivity activity);
//    void inject(ItemShotActivity activity);
    void inject(AddAdsActivity activity);

    void inject(BaseFragment fragment);
    void inject (TimelineFragment fragment);
    void inject (CategoryFragment fragment);
    void inject (FavoritesFragment fragment);
    void inject (ProfileFragment fragment);

}

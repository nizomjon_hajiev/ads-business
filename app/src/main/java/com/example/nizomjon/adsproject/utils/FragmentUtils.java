package com.example.nizomjon.adsproject.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by Nizomjon on 11/18/16.
 */

public class FragmentUtils {

    public void replaceFragment(int container, FragmentManager manager, Fragment fragment) {
        manager.beginTransaction().replace(container, fragment).commitAllowingStateLoss();
    }
}

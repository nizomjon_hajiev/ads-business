package com.example.nizomjon.adsproject.module.login.login;

import com.example.nizomjon.adsproject.mvp.MvpView;

/**
 * Created by Nizomjon on 11/18/16.
 */
public interface LoginView extends MvpView {

    void onLoginSuccess();

    void closeApplication();
}

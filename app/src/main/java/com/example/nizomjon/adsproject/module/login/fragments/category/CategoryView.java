package com.example.nizomjon.adsproject.module.login.fragments.category;

import com.example.nizomjon.adsproject.model.Flower;
import com.example.nizomjon.adsproject.mvp.MvpView;

import java.util.List;

/**
 * Created by Nizomjon on 11/23/16.
 */

public interface CategoryView extends MvpView {

    void showCategories(List<Flower> flowers);

}

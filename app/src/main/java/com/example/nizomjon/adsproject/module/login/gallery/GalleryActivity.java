//package com.example.nizomjon.adsproject.module.login.gallery;
//
//import android.content.Intent;
//import android.media.Image;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.widget.TextView;
//
//import com.example.nizomjon.adsproject.R;
//
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//
//
///**
// * Created by Nizomjon on 11/29/16.
// */
//
//public class GalleryActivity extends AppCompatActivity {
//
//    private static final int REQUEST_IMAGE = 2;
//    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
//    protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
//    private ArrayList<Image> images;
//    private int REQUEST_CODE_PICKER = 2000;
////    @BindView(R.id.recycler_view)
////    RecyclerView recyclerView;
//    @Nullable
//    @BindView(R.id.noItems)
//    TextView noItems;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_gallery);
//        ButterKnife.bind(this);
//        init();
//
//        start();
//    }
//
//    public void start() {
//        ImagePicker.create(this)
//                .folderMode(true) // set folder mode (false by default)
//                .folderTitle("Folder") // folder selection title
//                .imageTitle("Tap to select") // image selection title
//                .single() // single mode
//                .multi() // multi mode (default mode)
//                .limit(10) // max images can be selected (999 by default)
//                .showCamera(true) // show camera or not (true by default)
//                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
//                .origin(images) // original selected images, used in multi mode
//                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
//    }
//
//    public void startWithIntent() {
//        Intent intent = new Intent(this, ImagePickerActivity.class);
//
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_MODE, true);
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_MODE, ImagePickerActivity.MODE_MULTIPLE);
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_LIMIT, 10);
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SHOW_CAMERA, true);
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES, images);
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_FOLDER_TITLE, "Album");
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_TITLE, "Tap to select images");
//        intent.putExtra(ImagePickerActivity.INTENT_EXTRA_IMAGE_DIRECTORY, "Camera");
//        startActivityForResult(intent, REQUEST_CODE_PICKER);
//    }
//
//    private void init() {
////        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
////        recyclerView.setLayoutManager(gridLayoutManager);
//    }
//
////    @Override
////    public void setContentView(@LayoutRes int layoutResID) {
////        super.setContentView(R.layout.activity_gallery);
////    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
//            images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
//            StringBuilder sb = new StringBuilder();
//            for (int i = 0, l = images.size(); i < l; i++) {
//                sb.append(images.get(i).getPath() + "\n");
//            }
//            noItems.setText(sb.toString());
//        }
//    }
//
//
////    @Override
////    public int getLayout() {
////        return R.layout.activity_gallery;
////    }
//
//
//}

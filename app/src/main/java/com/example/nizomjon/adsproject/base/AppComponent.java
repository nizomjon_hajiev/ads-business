package com.example.nizomjon.adsproject.base;

import com.example.nizomjon.adsproject.utils.AppUtils;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 11/18/16.
 */
@Singleton
@Component(modules = {UtilsModule.class})
public interface AppComponent {

    void inject(AppUtils utils);
}

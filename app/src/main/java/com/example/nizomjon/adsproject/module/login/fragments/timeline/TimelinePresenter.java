package com.example.nizomjon.adsproject.module.login.fragments.timeline;

import android.content.Context;

import com.example.nizomjon.adsproject.api.NetModule;
import com.example.nizomjon.adsproject.api.RestService;
import com.example.nizomjon.adsproject.base.DaggerPresenterComponent;
import com.example.nizomjon.adsproject.model.Flower;
import com.example.nizomjon.adsproject.module.login.ApiCallback;
import com.example.nizomjon.adsproject.module.login.SubscriberCallback;
import com.example.nizomjon.adsproject.mvp.BasePresenter;
import com.example.nizomjon.adsproject.utils.AppUtils;
import com.example.nizomjon.adsproject.utils.PreferenceManager;
import com.example.nizomjon.adsproject.utils.UtilsModule;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Scheduler;

/**
 * Created by Nizomjon on 11/21/16.
 */

public class TimelinePresenter extends BasePresenter<TimelineView> {


    @Inject
    @Named("main_thread")
    Scheduler mMainThread;

    @Inject
    @Named("new_thread")
    Scheduler mNewThread;

    @Inject
    RestService api;

    @Inject
    PreferenceManager mprefs;

    @Inject
    AppUtils mUtils;


    public TimelinePresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context)).build().inject(this);
    }

    void getList() {
        getMvpView().showProgress();
        unSubscribeAll();
        subscribe(api.getFlowers(), new SubscriberCallback<>(new ApiCallback<List<Flower>>() {
            @Override
            public void onSuccess(List<Flower> model) {
                getMvpView().showList(model);
            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onCompleted() {
                getMvpView().hideProgress();
            }

            @Override
            public void onNetworkError() {

            }
        }));

    }

}

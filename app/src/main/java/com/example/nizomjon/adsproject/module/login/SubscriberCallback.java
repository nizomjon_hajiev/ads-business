package com.example.nizomjon.adsproject.module.login;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 20.08.2016.
 */
// SubscribeCallback is general callback for all observables.
public class SubscriberCallback<T> extends Subscriber<T> {

    private ApiCallback<T> mApiCallback;

    public SubscriberCallback(ApiCallback<T> apiCallback) {
        mApiCallback = apiCallback;
    }

    @Override
    public void onCompleted() {
        mApiCallback.onCompleted();
    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {




        }
//        if (e instceof Callback<T>) {
////            if (((RetrofitError) e).getKind() == RetrofitError.Kind.NETWORK) {
////                mApiCallback.onNetworkError();
////            } else {
////                mApiCallback.onFailure(((RetrofitError) e).getResponse().getStatus(), e.getMessage());
////            }
//        }

//        mApiCallback.onCompleted();

    }

    @Override
    public void onNext(T t) {
        mApiCallback.onSuccess(t);
    }


}

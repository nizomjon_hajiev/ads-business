package com.example.nizomjon.adsproject.mvp;

/**
 * Created by Nizomjon on 11/18/16.
 */

public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();

}
